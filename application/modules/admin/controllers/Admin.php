<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('login')){
			redirect("login");
		}
    }
	public function index()
	{
		$this->load->view('header');
		$this->load->view('v_index');
		$this->load->view('footer');
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
}
