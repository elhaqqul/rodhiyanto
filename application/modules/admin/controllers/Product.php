<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('login')){
			redirect("login");
		}
    }
	public function index()
	{
		$data['product'] = $this->db->select('productid,productname')->get('tbl_product')->result_object();
		$this->load->view('header',['page'=>'Create Product']);
		$this->load->view('form',$data);
		$this->load->view('footer');
	}

	function generatevoucher($length = 10){
		$perfix_voucher = "VC";
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $perfix_voucher.$randomString;
	}

	function postproduct(){
		$data = $this->input->post(['nik','product','method','diskon'],TRUE);
		$x = false;
		$v = null;
		do{
			$v = $this->generatevoucher(5);
			$cek = $this->db->get_where('tbl_marketing_discount',['vouchercode'=>$v]);
			if(!$cek->num_rows())
				$x = true;
		}while(!$x);
		$insert = [
			'vouchercode'=>$v,
			'marketingnik'=>$data['nik'],
			'paymentmethodid'=>$data['method'],
			'discount'=>$data['diskon'],
			'productid'=>$data['product'],
			'publish'=>$data['publish']
		];
		$insert = $this->db->insert('tbl_marketing_discount',$insert);
		$this->session->set_flashdata('success','Insert Successfully');
		redirect('admin/product');
	}

	function productlist(){
		$nik = $this->session->nik;
		$this->db->select('prd_diskon.id, prd_diskon.publish, prd.productname,method.satuan, method.jumlah, prd_diskon.discount,prd_diskon.vouchercode,prd_diskon.marketingnik');
		$this->db->from('tbl_marketing_discount as prd_diskon');
		$this->db->join('tbl_product as prd','prd.productid = prd_diskon.productid');
		$this->db->join('tbl_payment_method as method','method.id = prd_diskon.paymentmethodid');
		$this->db->where('prd_diskon.marketingnik',$this->session->nik);
		$result = $this->db->get();
		$data['product'] = $result;
		$this->load->view('header',['page'=>'List Product']);
		$this->load->view('list',$data);
		$this->load->view('footer');

	}

	function getpayment(){
		$id = $this->input->post('id');
		$this->db->select('tbl_payment_method.*');
		$this->db->from('tbl_producttypedetail');
		$this->db->join('tbl_payment_method','tbl_payment_method.id = tbl_producttypedetail.paymentmethodid');
		$this->db->where('tbl_producttypedetail.productid',$id);
		$this->db->group_by(['tbl_payment_method.id','tbl_payment_method.jumlah','tbl_payment_method.satuan']);
		$data = $this->db->get();
		if($data->num_rows()){
			echo json_encode(['data'=>$data->result_object()]);
		}else{
			echo json_encode(['data'=>null]);
		}
	}

	// function productdelete($id){
	// 	$this->db->where('id',$id);
	// 	$delete = $this->db->delete('tbl_marketing_product_diskon');
	// 	if($delete){
	// 		$this->session->set_flashdata('success','Delete Successfully');
	// 		redirect('admin/product/productlist');
	// 	}else{
	// 		$this->session->set_flashdata('error','Delete Invalid');
	// 		redirect('admin/product/productlist');
	// 	}
	// }

	function updatestatus($type,$id){
		$this->db->where('id',$id);
		$stt = $type == 'publish' ? '1' : '0';
		$update = $this->db->update('tbl_marketing_discount',['publish'=>$stt]);
		if($update){
			$this->session->set_flashdata('success','Update Successfully');
			redirect('admin/product/productlist');
		}else{
			$this->session->set_flashdata('error','Update Error');
			redirect('admin/product/productlist');
		}
	}
}
