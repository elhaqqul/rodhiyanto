<div class="row">
    <div class="col-md-12">
        <div class="white-box">
        	<?php
        		if($this->session->flashdata('success')){
        			echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        		}
        	?>
        	<div style="margin-bottom: 10px;">
        		<a href="<?php echo base_url('admin/product/productlist');?>" class="btn btn-primary">List</a>
        	</div>
			<form action="<?php echo base_url('admin/product/postproduct');?>" method="post">
			  <input type="hidden" name="nik" value="<?php echo $this->session->nik;?>">
			  <div class="form-group">
			  	<label for="exampleInputEmail1">Product</label>
			  	<select class="form-control" name="product" required onchange="prd(this.value);">
			  		<option value="">--Select--</option>
			  		<?php
			  			foreach($product as $p){
			  		?>
			  				<option value="<?php echo $p->productid;?>"><?php echo $p->productname;?></option>
			  		<?php
			  			}
			  		?>
			  	</select>
			  </div>
			  <div class="form-group">
			  	<label for="exampleInputEmail1">Payment Method</label>
			  	<select class="form-control" name="method" required>
			  		<option value="">--Select--</option>
			  	</select>
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1">Discount (in %)</label>
			    <input type="text" name="diskon" class="form-control" onkeydown="return num(event);" placeholder="example : 10">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1">Status</label>
			    <select name="publish" class="form-control">
			    	<option value="1">Publish</option>
			    	<option value="0">Unpublish</option>
			    </select>
			  </div>
			  <button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	function prd(t){
		$.post("<?php echo base_url('admin/product/getpayment');?>",{id:t},function(res){
			if(res.data){
				var htm = '';
				$.each(res.data,function(k,v){
					htm+='<option value="'+v.id+'">'+v.jumlah+' '+v.satuan+'</option>';
				});
				if(htm != '')
					$('[name="method"]').html(htm);
			}
		},'json');
	}

	function num(e){
		var nm = [48,49,50,51,52,53,54,55,56,57,8,190,9];
		if(jQuery.inArray(e.keyCode,nm) !== -1){
			return true;
		}else{
			return false;
		}
	}
</script>


