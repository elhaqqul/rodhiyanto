<div class="row">
    <div class="col-md-12">
        <div class="white-box">
        	<?php
        		if($this->session->flashdata('success')){
        			echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
        		}
        	?>
        	<?php
        		if($this->session->flashdata('error')){
        			echo '<div class="alert alert-success">'.$this->session->flashdata('error').'</div>';
        		}
        	?>
        	<div style="margin-bottom: 10px;">
        		<a href="<?php echo base_url('admin/product');?>" class="btn btn-primary">Create</a>
        	</div>
        	<div class="table table-responsive">
        		<table class="table" id="dtable">
        			<thead>
        				<tr>
        					<th>NIK</th>
        					<th>Product</th>
        					<th>Method</th>
        					<th>Vocher Code</th>
        					<th>Discount</th>
        					<th>Status</th>
        					<th>Action</th>
        				</tr>
        			</thead>
        			<tbody>
        				<?php
        					if($product->num_rows()){
        						foreach($product->result_object() as $prd){
        							$type = $prd->publish == '1' ? 'unpublish' : 'publish';
        				?>
        					<tr>
        						<td><?php echo $prd->marketingnik;?></td>
        						<td><?php echo $prd->productname;?></td>
        						<td><?php echo $prd->jumlah.' '.$prd->satuan;?></td>
        						<td><?php echo $prd->vouchercode;?></td>
        						<td><?php echo $prd->discount.' % ';?></td>
        						<td><?php echo ($prd->publish == '1') ? 'Publish' : 'Unpublish';?></td>
        						<td>
        							<a data-toggle="tooltip" title="Update To <?php echo ucfirst($type);?>" href="<?php echo base_url("admin/product/updatestatus/$type/$prd->id");?>" onclick="return updateData();" class="btn btn-danger"><?php echo ucfirst($type);?></a>
        						</td>
        					</tr>
        				<?php
        					}
        					}else{
        						echo '<tr><td colspan="5" align="center">Not Found</td></tr>';
        					}
        				?>
        			</tbody>
        		</table>
        	</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#dtable').DataTable();
	});
	

	function updateData(){
		var c = confirm('Are You Sure?');
		if(c)
			return true;
		return false;
	}
</script>