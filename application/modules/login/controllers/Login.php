<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        if($this->session->userdata('login')){
			redirect(base_url("admin"));
		}
    }
	public function index()
	{
		$this->load->view('login');
	}

	function postlogin(){//admedika
		$input = $this->input->post(['nik','password'],TRUE);
		$data = $this->db->get_where('tbl_marketing',['nik'=>$input['nik'],'status_active'=>'3']);
		$cek = $data->num_rows();
		if($cek){
			if(password_verify($input['password'], $data->result_array()[0]['password'])){
				$login_time = date('Y-m-d H:i:s');
				$id = $data->result_array()[0]['id'];
				$update['active_since'] = $login_time;
				$this->db->where('nik',$id);
				$this->db->update('tbl_marketing',$update);
				$login=[
					'nik'=>$data->result_array()[0]['nik'],
					'fullname'=>$data->result_array[0]['fullname'],
					'email'=>$data->result_array[0]['email'],
					'phonenumber'=>$data->result_array[0]['phonenumber'],
					'login'=>true,
					'login_time'=>$login_time
				];
				$this->session->set_userdata($login);
				redirect("admin");
			}else{
				$this->session->set_flashdata('error','NIK Not Found');
				redirect('login');
			}
		}else{
			$this->session->set_flashdata('error','Login Failed');
			redirect('login');
		}
	}
}
